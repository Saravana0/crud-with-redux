import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory,Link  } from 'react-router-dom';
import { toast } from 'react-toastify';


function AddUser() {
    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [age,setAge] = useState("");
    const [number,setNumber] = useState("");

    const userDetails = useSelector (state => state);
    // console.log(userDetails);
    const dispatch = useDispatch();

    const history = useHistory();

    const handleSubmit = (e)=>{
        e.preventDefault();

        const checkEmail = userDetails.find((userDetails) => userDetails.email === email && email);
        const checkNumber = userDetails.find((userDetails) => userDetails.number === parseInt(number) && number);


        if(!email || !name || !age || !number){
            return toast.warning("Please fill in all fields!");
        }

        if(checkEmail) {
            return toast.error("This email already Exists!");
        }

        if(checkNumber) {
            return toast.error("This number already Exists!");
        }

        const data = {
            id: userDetails[userDetails.length - 1].id + 1,
            name,
            email,
            age,
            number
        }

        // console.log(data);
        dispatch({type: "ADD_USER", payload:data});
        toast.success("User added successfully!!");
        history.push("/userlist");
    };

  return (
    <div className='container'>
        <div className='row'>
        <div className='col-md-11 text-md-end mt-5'>
                <Link to="/userlist" className='btn btn-outline-primary btn-dark text-white'>
                    Show User Lists
                </Link>
            </div>
            <h1 className='display-3 my-1 text-center'>
                Add user
            </h1>
            <div className='col-md-6 shadow mx-auto p-5'>
                <form onSubmit={handleSubmit}>
                    <div className='form-group'>
                        <input type="text" placeholder="Name" className='form-control' 
                        value={name} onChange={e=> setName(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="text" placeholder="Email" className='form-control'
                        value={email} onChange={e=> setEmail(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="text" placeholder="Age" className='form-control'
                       value={age} onChange={e=> setAge(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="number" placeholder="Phone number" className='form-control'
                        value={number} onChange={e=> setNumber(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="submit" value="Add User" className='btn btn-block btn-dark col-md-12'/>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
  )
}

export default AddUser