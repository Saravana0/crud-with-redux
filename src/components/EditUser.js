import {React, useEffect, useState} from 'react'
import { Link, useParams, useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { toast } from 'react-toastify';

function EditUser() {
    const [name,setName] = useState("");
    const [email,setEmail] = useState("");
    const [age,setAge] = useState("");
    const [number,setNumber] = useState("");

    const {id} = useParams();

    const history = useHistory();
    const dispatch = useDispatch();

    const userDetails = useSelector (state => state);
    const currentUser = userDetails.find(userDetails=> userDetails.id === parseInt(id));

    useEffect (() => {
        if (currentUser) {
            setName(currentUser.name);
            setEmail(currentUser.email);
            setAge(currentUser.age);
            setNumber(currentUser.number);
        }
    },[currentUser]);

    const handleSubmit = (e)=>{
        e.preventDefault();

        const checkEmail = userDetails.find(
            (userDetails) => userDetails.id !== parseInt(id) && userDetails.email === email && email
            );
        const checkNumber = userDetails.find(
            (userDetails) => userDetails.id !== parseInt(id) && userDetails.number === parseInt(number) && number
            );


        if(!email || !name || !age || !number){
            return toast.warning("Please fill in all fields!");
        }

        if(checkEmail) {
            return toast.error("This email already Exists!");
        }

        if(checkNumber) {
            return toast.error("This number already Exists!");
        }

        const data = {
            id: parseInt(id),
            name,
            email,
            age,
            number
        }

        // console.log(data);
        dispatch({type: "UPDATE_USER", payload:data});
        toast.success("User Updated successfully!!");
        history.push("/userlist");
    };


  return (
    <div className='container'>
            {currentUser ? (
                <>
            <h1 className='display-3 my-5 text-md-center'>
                Edit user {id}
            </h1>
            <div className='row'>
            <div className='col-md-6 shadow mx-auto p-5'>
                <form onSubmit={handleSubmit}>
                    <div className='form-group'>
                        <input type="text" placeholder="Name" className='form-control'
                        value={name} onChange={e=> setName(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="text" placeholder="Email" className='form-control'
                        value={email} onChange={e=> setEmail(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="number" placeholder="Age" className='form-control'
                        value={age} onChange={e=> setAge(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="number" placeholder="Phone number" className='form-control'
                        value={number} onChange={e=> setNumber(e.target.value)}/>
                    </div>
                    <div className='form-group mt-3'>
                        <input type="submit" value="Update User" className='btn btn-dark'/>
                        <Link to="/" className='btn btn-dark bg-danger ms-2'> Cancel </Link>
                    </div>
                    
                </form>
            </div>
        </div>
        </>
        ) : <h1 className='display-3 my-5 text-md-center'> User with id {id} not exits </h1>}
        
    </div>
  )
}

export default EditUser