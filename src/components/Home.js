import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { toast } from 'react-toastify';

const Home = () => {

    const userDetails = useSelector (state => state);

    const dispatch = useDispatch();

    const deleteUser = (id) =>{
        dispatch({type: "DELETE_CONTACT",payload:id});
        toast.success("Contact deleted successfully")
    }

  return (
    <div className='container'>
        <div className='row'>
            <div className='col-md-11 text-md-end mt-5 mb-3'>
                <Link to="/" className='btn btn-outline-dark'>
                    Add User
                </Link>
            </div>
            <div className='col-md-10 mx-auto mt-4'>
               <table className='table table-hover'>
                    <thead className='text-white bg-dark text-center'>
                        <tr>
                            <th scope='col'>#</th>
                            <th scope='col'>Name</th>
                            <th scope='col'>Email</th>
                            <th scope='col'>Age</th>
                            <th scope='col'>Number</th>
                            <th scope='col'>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {userDetails.map((userDetails,id) => (
                            <tr key={id} className="text-center align-middle">
                                <td>{id + 1}</td>
                                <td>{userDetails.name}</td>
                                <td>{userDetails.email}</td>
                                <td>{userDetails.age}</td>
                                <td>{userDetails.number}</td>
                                <td>
                                    <Link to={`/edit/${userDetails.id}`} className="btn btn-small btn-primary ">
                                        Edit
                                    </Link>
                                    <button type="button" onClick={()=>deleteUser(userDetails.id)} className="btn btn-small btn-danger ms-2">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
               </table>
            </div>
        </div>
        
    </div>
  )
}

export default Home