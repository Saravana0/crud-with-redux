import React from 'react';
import { ToastContainer } from "react-toastify";
import {Switch, Route} from 'react-router-dom';

import './App.css';
import Navbar from './components/Navbar';
import Home from './components/Home';
import AddUser from './components/AddUser';
import EditUser from './components/EditUser';

const App = () => {
  return (
    <div className="App">
      <ToastContainer />
      <Navbar />
      <Switch>
        <Route exact path="/" component={() => <AddUser />}/>
         
        
        <Route path="/userlist" >
          <Home/>
        </Route>
        <Route path="/edit/:id">
         <EditUser />
        </Route>
      </Switch>
     
      
    </div>
  );
}

export default App;
