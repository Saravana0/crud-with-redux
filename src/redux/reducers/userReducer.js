const initialState = [
    {
        id: 0,
        name: "Peter",
        email: "peter@gmail.com",
        age: 28,
        number: 1234567890,
    },
    {
        id: 1,
        name: "Nobita",
        email: "nobita@gmail.com",
        age: 19,
        number: 9087654321,
    },
    {
        id: 2,
        name: "Hulk",
        email: "Hulk@gamil.com",
        age: 31,
        number: 8090634567,
    },
    {
        id: 3,
        name: "Joker",
        email: "Joker@gamil.com",
        age: 38,
        number: 6056890348,
    },
];

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_USER":
            state = [...state, action.payload];
        case "UPDATE_USER":
            const UpdateState = state.map((userDetails) => 
            userDetails.id === action.payload.id ? action.payload : userDetails
            );
            state = UpdateState;
            return state;
        case "DELETE_CONTACT":
            const filterUsers = state.filter(
                (userDetails)=> userDetails.id !== action.payload && userDetails
                );
            state = filterUsers;
            return state;
        default:
            return state;
    }
}

export default userReducer;